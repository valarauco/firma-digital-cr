# Script para Firma Digital

Script para instalar las bibliotecas y certificados de Firma Digital de Costa Rica; basado en las guías de https://fran.cr/

Sistemas soportados:
* Debian 9 y nuevos
* Ubuntu 18.04 y nuevos

***Nota***: En Debian 11 se presentan problemas debido a que eliminaron el binario *rename.ul* del sistema.

Actualmente se ha realizado pruebas con:
* Debian 10
* Debian 11
* Ubuntu 18.04

## Requerimientos

Se requiere tener descargado previamente el "instalador" de [Soporte Firma Digital de Costa Rica](https://www.soportefirmadigital.com/)

Se ha realizado pruebas con el "instalador" en su revisión 12 (sfd_ClientesLinux_DEB64_Rev.12.zip) y revisión 16 (sfd_ClientesLinux_DEB64_Rev.16.zip).

## Ejecución

`sudo ./firma_digital.sh <ruta al archivo sfd_ClientesLinux_XXXX.zip> `