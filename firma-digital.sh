#! /bin/bash

. /etc/os-release
SDFPATH="$1"
BASEDIR="$(dirname $0)"
TMPPATH="/tmp/firma-digital"
OSNAME=""
OSRELEASE=""

echoerr() { echo "$@" 1>&2; }

if [ ! -f "$SDFPATH" ] || [ -z "$(file $SDFPATH | grep 'Zip archive')"  ]; then
     echoerr "Debe especificar el path al archivo de Soporte Firma Digital de " \
         "Costa Rica (ej. sfd_ClientesLinux_DEB64_Rev.12.zip)"
    exit 1
fi

case $ID in
    "debian")
        OSNAME=$ID
        case $(cat /etc/debian_version) in
            "bookworm/sid")
                OSRELEASE="12"
                ;;
            "bullseye")
                OSRELEASE="11"
                ;;
            "buster"|"10"*)
                OSRELEASE="10"
                ;;
        esac
        ;;
    "ubuntu")
        OSNAME=$ID
        case $VERSION_ID in
            "18.04")
                OSRELEASE="1804"
                ;;
            *)
                OSRELEASE="1810"
                ;;
        esac
        ;;
    "elementary")
        OSNAME=$ID_LIKE
        OSRELEASE="1804"
        ;;
esac

if [ -z "$OSRELEASE" ]; then
    echoerr "Sistema no soportado: $PRETTY_NAME"
    exit 1
fi

echo
echo "Configurador de Firma Digital de Costa Rica"
echo "  Basado en las guías de https://fran.cr/"
echo

echo "Sistema detectado: $OSNAME $OSRELEASE"
echo "Instalador: $SDFPATH"
echo

echo "Instalando dependencias."
#sudo apt -qq update && sudo apt -y install pcscd bubblewrap \
#      icedtea-netx binutils sudo
#[[ $? -gt 0 ]] &&  echoerr "Error al instalar dependencias" && exit 1


echo "Descomprimiendo instalador $SDFPATH."
mkdir -pv "$TMPPATH" && unzip "$SDFPATH" -d "${TMPPATH}/" \
    && find "${TMPPATH}" -type d -name "Firma Digital" \
        -exec cp -a '{}' "${TMPPATH}/" \;
[[ $? -gt 0 ]] && echoerr "Error al descomprimir instalador" && exit 1


echo "Instalando certificados de BCCR."
sudo cp -pv "${TMPPATH}/Firma Digital/Certificados/"* \
      "/usr/local/share/ca-certificates/" && \
for file in /usr/local/share/ca-certificates/*.cer; do
  sudo mv -v "${file}" "${file%.cer}.crt"
done 2>/dev/null && \
for file in /usr/local/share/ca-certificates/*.crt; do 
  sudo openssl x509 -inform DER -in "${file}" -out "${file}.tmp"
  sudo mv "${file}.tmp" "${file}"
done 2>/dev/null && \
sudo find /usr/local/share/ca-certificates/ -type f -empty -delete && \
sudo update-ca-certificates --fresh
[[ $? -gt 0 ]] && echoerr "Error al instalar certificados BCCR" && exit 1

echo "Descomprimiendo biblioteca libASEP11.so (módulo PKCS#11)."
ar p "${TMPPATH}/Firma Digital/PinTool/IDProtect PINTool 7.24.02/DEB/idprotectclient_7.24.02-0_amd64.deb" \
  data.tar.gz | tar zx -O ./usr/lib/x64-athena/libASEP11.so > ${TMPPATH}/libASEP11.so
[[ $? -gt 0 ]] && echoerr "Error al descomprimir biblioteca libASEP11.so" && exit 1

echo "Instalando biblioteca libASEP11.so (módulo PKCS#11)."
sudo cp -pv ${TMPPATH}/libASEP11.so /usr/lib/x86_64-linux-gnu/ \
    && sudo chown root:root /usr/lib/x86_64-linux-gnu/libASEP11.so \
    && sudo mkdir -pv /usr/lib/x64-athena/ \
    && sudo mkdir -pv /Firma_Digital/LIBRERIAS/ \
    && sudo ln -sfv /usr/lib/x86_64-linux-gnu/libASEP11.so /usr/lib/x64-athena/ \
    && sudo ln -sfv /usr/lib/x86_64-linux-gnu/libASEP11.so /usr/lib/ \
    && sudo ln -sfv /usr/lib/x86_64-linux-gnu/libASEP11.so /usr/local/lib/ \
    && sudo ln -sfv /usr/lib/x86_64-linux-gnu/libASEP11.so /Firma_Digital/LIBRERIAS/ \
    && sudo ln -sfv /usr/local/share/ca-certificates /Firma_Digital/CERTIFICADOS
[[ $? -gt 0 ]] && echoerr "Error al instalando biblioteca libASEP11.so" && exit 1

echo "Copiando configuración de libASEP11.so."
sudo mkdir -pv /etc/Athena/ \
    && sudo cp -v "$BASEDIR/files/IDPClientDB.xml" /etc/Athena/IDPClientDB.xml
[[ $? -gt 0 ]] && echoerr "Error al copiar configuración de biblioteca libASEP11.so" && exit 1


echo "Configurando módulos y p11-kit para $OSNAME $OSRELEASE."
case $OSRELEASE in
    "9")
        sudo cp -v "$BASEDIR/files/firma-digital.module.deb9" \
            /usr/share/p11-kit/modules/firma-digital.module
        ;;
    *)
        sudo cp -v "$BASEDIR/files/firma-digital.module.deb10" \
            /usr/share/p11-kit/modules/firma-digital.module
        ;;

esac
case $OSRELEASE in
    "10"|"11"|"1810")
        echo "Habilitando p11-kit-proxy en el archivo p11-kit-trust.module" && \
        sudo sed -i 's/disable-in: p11-kit-proxy/#disable-in: p11-kit-proxy/' \
                /usr/share/p11-kit/modules/p11-kit-trust.module
    ;;
esac

echo "Configurando p11-kit general"
sudo cp -v "$BASEDIR/files/update-p11-kit-symlinks" \
      /usr/local/sbin/update-p11-kit-symlinks
sudo chmod +x /usr/local/sbin/update-p11-kit-symlinks
sudo cp -v "$BASEDIR/files/p11-kit-proxy-updater.service" \
      /etc/systemd/system/p11-kit-proxy-updater.service
sudo systemctl enable --now p11-kit-proxy-updater.service

echo
echo "Listo. El proceso ha finalizado."
echo "Se recomienda reiniciar el navegador y cualquier otro servicio que" \
     "utilice los certificados digitales"
#read

exit 0

